package GeoMessage;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.sql.PreparedStatement;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author fch7873
 */
@WebServlet(urlPatterns = {"/Login"})
public class Login extends HttpServlet {
    
    private static Connection conn;
    private static PreparedStatement insertStmt;
    private static PreparedStatement checkUserStmt;
    private static PreparedStatement maxMessageID;
    private static ResultSet rs, rsID;
    
    public Login() throws InvalidPropertiesFormatException, IOException, ClassNotFoundException{
        try {
            //At the moment connects stright to the uni db
            Properties properties = new Properties();
            properties.loadFromXML(getClass().getResourceAsStream("userConfig.xml"));
            String dbDriver = properties.getProperty("dbDriver");
            String dbUrl = properties.getProperty("dbUrl");
            String dbTable = properties.getProperty("dbTable");
            String dbAId = properties.getProperty("dbAId");
            String dbUser = properties.getProperty("dbUser");
            String dbTime = properties.getProperty("dbTime");
            String user = properties.getProperty("user");
            String password = properties.getProperty("password");
            try {
                Class.forName(dbDriver);
                DriverManager.setLoginTimeout(4);
                conn = DriverManager.getConnection(dbUrl, user, password);
                
            } catch (SQLException ex) {
                System.out.println("Properties error");
            }
            insertStmt = conn.prepareStatement("INSERT INTO " + dbTable + " ( " + dbUser + ", " + dbAId
                        + ") VALUES (?, ?)");
            checkUserStmt = conn.prepareStatement("SELECT * FROM " + dbTable + " WHERE " + dbUser + "=? And "
                    + dbAId + "= ?");
            maxMessageID = conn.prepareStatement("SELECT MAX(message_id) FROM messages");
        } catch (SQLException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        
        String name = (String) request.getParameter("name");
        String aId = (String) request.getParameter("aId");
        String regex = "[A-Za-z0-9_]{5,16}";
        String messageResponse = null;
        long maxId = -1;
        if(aId != null && name != null && name.matches(regex)){
                try {
                   insertStmt.setString(1, name);
                   insertStmt.setString(2, aId);
                   insertStmt.executeUpdate();
                   System.out.println("User created");
                   messageResponse = "Accept"; 
                   rsID = maxMessageID.executeQuery();
                   rsID.next();
                   maxId = rsID.getLong(1);
                   System.out.println("MaxID=" + maxId);
                } catch (SQLException ex) {
                    System.out.println("Exception:" + ex.getMessage());
                    try {
                        checkUserStmt.setString(1, name);
                        checkUserStmt.setString(2, aId);
                        rs = checkUserStmt.executeQuery();
                        if(rs.next()) {
                            System.out.println("Successfully logged in");
                            messageResponse = "Accept";
                            rsID = maxMessageID.executeQuery();
                            rsID.next();
                            maxId = rsID.getLong(1);
                        } else {
                            System.out.println("User name registered");
                            messageResponse = "User Taken";
                        }
                    } catch (SQLException ex1) {
                        System.out.println("Exception:" + ex1.getMessage());
                    }
                }
            } else {
                System.out.println("Could not log in");
                messageResponse = "Could not log in";
            }
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.println("<!DOCTYPE html>");
        out.println("<!DOCTYPE html>");
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Servlet system</title>");            
        out.println("</head>");
        out.println("<body>");
        out.println("<div>" + messageResponse + "</div>");
        if(maxId != -1){
            out.println("<div>" + maxId + "</div>");
        }
        out.println("</body>");
        out.println("</html>");
    }
        

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
