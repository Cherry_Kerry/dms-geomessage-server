/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GeoMessage;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author fch7873
 */
@WebServlet(name = "AddMessage", urlPatterns = {"/AddMessage"})
public class AddMessage extends HttpServlet {

    private static Connection conn;
    private static PreparedStatement insertStmt;
    
    public AddMessage() throws IOException, ClassNotFoundException, SQLException{
        
            //At the moment connects stright to the uni db
            System.out.println("hello world");
            Properties properties = new Properties();
            properties.loadFromXML(getClass().getResourceAsStream("messageConfig.xml"));
            String dbDriver = properties.getProperty("dbDriver");
            String dbUrl = properties.getProperty("dbUrl");
            String dbTable = properties.getProperty("dbTable");
            String dbUser = properties.getProperty("dbUser");
            String dbMessage = properties.getProperty("dbMessage");
            String dbLatitude = properties.getProperty("dbLatitude");
            String dbLongitude = properties.getProperty("dbLongitude");
            String user = properties.getProperty("user");
            String password = properties.getProperty("password");
        try {     
            Class.forName(dbDriver);
            DriverManager.setLoginTimeout(4);
            conn = DriverManager.getConnection(dbUrl, user, password);
        } catch (SQLException ex) {
            System.out.println("SQL connecttion error");
        }
        insertStmt = conn.prepareStatement("INSERT INTO " + dbTable + " ( " + dbUser + ", " + dbMessage +  ", "
                + dbLatitude + ", " + dbLongitude + ") VALUES (?, ?, ?, ?)");
        

    }
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String name = request.getParameter("name");
        String msg = request.getParameter("msg");
        String latitude = request.getParameter("latitude");
        String longitude = request.getParameter("longitude");
        System.out.println(name + " " + msg + " "+ latitude + " " + longitude);
        String messageResponce = null;
        if(name != null && msg != null && latitude != null & longitude != null && msg.length() <= 255){ 
            try {
                if(msg.equals("test123")){
                    new AddMultiple();
                    messageResponce = "Added Multiple";
                } else {
                
                    insertStmt.setString(1, name);
                    insertStmt.setString(2, msg);
                    insertStmt.setString(3, latitude);
                    insertStmt.setString(4, longitude);
                    insertStmt.executeUpdate();
                    messageResponce = "Success";
                }
            } catch (SQLException ex) {
                System.out.println("SQL error");
                messageResponce = "Error with sql statment";
            }   
        } else {
            messageResponce = "Error adding message";
        }
        
        
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddMessage</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<div>" + messageResponce + "</div>");
            out.println("</body>");
            out.println("</html>");
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
        private static class AddMultiple extends Thread {
        
        private final double AUCKLAND_LAT = -36.850806;
        private final double AUCKLAND_LONG = 174.764791;
        private final double VARIANCE = 0.08;
        
        public AddMultiple() {
            start();
        }

        @Override
        public void run() {
               for(int i = 0; i < 10 ; i++){
                   try {
                       insertStmt.setString(1, "Random");
                       insertStmt.setString(2, "RandomMessage" + i);
                       insertStmt.setDouble(3, randomNum(AUCKLAND_LAT - VARIANCE, AUCKLAND_LAT + VARIANCE));
                       insertStmt.setDouble(4, randomNum(AUCKLAND_LONG - VARIANCE, AUCKLAND_LONG + VARIANCE));
                       insertStmt.executeUpdate();
                   } catch (SQLException ex) {
                       Logger.getLogger(AddMessage.class.getName()).log(Level.SEVERE, null, ex);
                   }
                   
               }
        }
        
        public synchronized double randomNum(double min, double max){
            return min + (max - min) * Math.random();
        }
    } 
}
