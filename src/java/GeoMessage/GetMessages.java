/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package GeoMessage;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author James
 */
@WebServlet(name = "GetMessages", urlPatterns = {"/GetMessages"})
public class GetMessages extends HttpServlet {

    private final double VARIANCE = 0.09;
    private static Connection conn;
    private static PreparedStatement messagesStmt;
    public GetMessages() throws ClassNotFoundException, IOException, SQLException{
        
        Properties properties = new Properties();
            properties.loadFromXML(getClass().getResourceAsStream("messageConfig.xml"));
            String dbDriver = properties.getProperty("dbDriver");
            String dbUrl = properties.getProperty("dbUrl");
            String dbTable = properties.getProperty("dbTable");
            String dbUser = properties.getProperty("dbUser");
            String dbMessage = properties.getProperty("dbMessage");
            String dbLatitude = properties.getProperty("dbLatitude");
            String dbLongitude = properties.getProperty("dbLongitude");
            String user = properties.getProperty("user");
            String password = properties.getProperty("password");
        try {     
            Class.forName(dbDriver);
            DriverManager.setLoginTimeout(4);
            conn = DriverManager.getConnection(dbUrl, user, password);
        } catch (SQLException ex) {
            System.out.println("SQL connecttion error");
        }
        messagesStmt = conn.prepareStatement("SELECT * FROM " + dbTable + " WHERE "
                + "message_id > ? AND "+ dbLatitude +" < ? AND " + dbLatitude + " > ? AND " + dbLongitude + " < ? AND "
                + dbLongitude + " > ? AND NOT " + dbUser + " = ?" );
        //messagesStmt = conn.prepareStatement("SELECT " + dbUser + ", " + dbMessage + " from "+ dbTable +" WHERE message_id > ?");
    }
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            response.setContentType("text/html;charset=UTF-8");
            try (PrintWriter out = response.getWriter()) {
                /* TODO output your page here. You may use following sample code. */
                out.println("<!DOCTYPE html>");
                out.println("<html>");
                out.println("<head>");
                out.println("<title>Servlet Send Messages</title>");
                out.println("</head>");
                out.println("<body>");
                
                String latString = request.getParameter("latitude");
                String longString = request.getParameter("longitude");
                String maxIdString = request.getParameter("maxId");
                
                System.out.println(latString + " " + longString + " " + maxIdString);
                
                if(latString != null && longString != null && maxIdString != null){
                    double latitude = Double.parseDouble(latString);
                    double longitude = Double.parseDouble(longString);
                    int maxId = Integer.parseInt(maxIdString);
                    String name = request.getParameter("name");

                    try {
                        messagesStmt.setInt(1, maxId);
                        messagesStmt.setDouble(2, latitude + VARIANCE);
                        messagesStmt.setDouble(3, latitude - VARIANCE);
                        messagesStmt.setDouble(4, longitude + VARIANCE);
                        messagesStmt.setDouble(5, longitude - VARIANCE);
                        messagesStmt.setString(6, name);

                        ResultSet rs = messagesStmt.executeQuery();
                        
                        //Setting the meta values of new maxx message id and num of messages
                        rs.last();
                        int size = rs.getRow();
                        int max = rs.getInt("message_id");
                        out.println("<div>"+ max +"</div>");
                        out.println("<div>"+ size +"</div>");
                        
                        //getting message data
                        rs.first();
                        do {
                            String meseageName = rs.getString("user");
                            String messageContent = rs.getString("message");
                            out.println("<div>" + meseageName + "</div>");
                            out.println("<div>" + messageContent  + "</div>");
                        } while(rs.next());
                    } catch (SQLException ex) {
                        Logger.getLogger(GetMessages.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
                out.println("</body>");
                out.println("</html>");
            }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
   
}
